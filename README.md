# K3S environment with vagrant

## Installation

Tuto : https://blog.filador.fr/a-la-decouverte-de-k3s/

### Issues with ansible

You have multiple options:

* Install the missing collection, when logged in with the problematic user:
  
    ```bash
    ansible-galaxy collection install community.general
    ```

* Install the full Ansible package:
  
    ```bash
    pip uninstall ansible-core ansible-base
    pip install ansible
    ```

* Reinstall the collection in the shared folder, with the option -p
  
    ```bash
    ansible-galaxy collection install community.general \
        -p /usr/share/ansible/collections
    ```

### Trucs utiles (enfin peut-être)

https://www.padok.fr/blog/kubernetes-gitlab-pipeline
